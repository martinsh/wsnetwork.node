/* 
 * File:   main.c
 * Author: Martins
 *
 * Created on otrdiena, 2014, 23 septembris, 22:58
 *
 * Interrupt driven Radio transmission with USART.
 */

#include <xc.h>

#include <stdio.h>
#include <stdlib.h>
#include "main.h"

#include "sensors.h"

// Configuration bits
#pragma config WDTE=0, FOSC=XT, LVP=0, MCLRE=1, PWRTE=0, DEBUG=0

// Payload TX cursor
uint8_t txPayloadCursor = 0;
uint8_t bPreambleSent = 0;

// Preamble TX cursor
uint8_t txPreambleCursor = 0;

const uint8_t preamble[5] = {0x55, 0x55, 0xFF, 0x00, 0xFE};

#if !defined(_XTAL_FREQ)
    #define _XTAL_FREQ 4000000
#endif

// State Machine states.
enum SM_STATES
{
    SM_IDLE = 0,
    SM_READY_TO_MEASURE,
    SM_READY_TO_ENCODE,
    SM_READY_TO_TX,
    SM_TX_IN_PROGRESS,
    SM_TX_DONE,
};

// State tracking variable. See "Cooperative Multitasking" in TCP/IP stach help.
enum SM_STATES appState;
/*
 * 
 */
int main(int argc, char** argv) {

    // For Timer1
    // The TMR1H:TTMR1L register pair and the TMR1IF bit should be cleared before enabling interrupts.
    TMR1 = 0;
    PIR1bits.TMR1IF = 0;

    /** IRQ **/
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1; // Enable all peripheral IRQs, incl USART, Timer1. Wihtout this, TXIF and other IRQ flags wont be triggered

    /** Timer 1. 16-bit. **/
    PIE1bits.TMR1IE = 1; // Enable Timer1 overflow interrupt
    T1CONbits.T1CKPS = 0b00; // Timer1 Input Clock Prescaler is 1:1
    T1CONbits.T1SYNC = 1; // Do not sync Timer1 External Clock Input with internal phase clock
    T1CONbits.TMR1CS = 1; // Source is External osc
    T1CONbits.T1OSCEN = 1; // Enable LP oscillator. External 352.768KHz crystal is needed between  T1OSO and T1OSI
    T1CONbits.TMR1GE = 0;

    __delay_ms(1);  // Allow low power oscillator to stabilize
    T1CONbits.TMR1ON = 1; // Start timer

    OPTION_REGbits.T0CS = 0; // TMR0 Clock Source is Internal instruction cycle clock (FOSC/4)
    OPTION_REGbits.PSA = 0; // Prescaler assigned to TMR0
    OPTION_REGbits.PS = 0b111; // Prescaler value = 1:256
    INTCONbits.T0IE = 0; // Enable Timer0 interrupt

    // External IRQ
    OPTION_REGbits.INTEDG = 0; // Ext IRQ on falling edge to avoid IRQ on startup (we have pull-up resistor on the INT pin)
    INTCONbits.INTE = 0; // Ext IRQ disable

    LED_TRIS = 0;
    LED = 1;

    // SET AN* pins as digital mode
    ANSELH = 0b00000000;

    // USART Setup
    TXSTA = 0b00000010; // TX off, Async mode. Low speed. p.164
    TXSTAbits.TXEN = 1; // Enable USART
    PIE1bits.TXIE = 0; // Disable TXIF on start
    
    
    RCSTAbits.SPEN = 1; // Serial port enable. This also sets TX pin as output

    BAUDCTLbits.BRG16 = 0; // 8-bit Baud Rate Generator is used p.166
    BAUDCTLbits.SCKP = 1; // Invert UART output. This keeps UART Idle low. Otherwise Transmitter will transmit all the time when idle.
    
    SPBRGH = 0x00;
    SPBRG = 5; // 1bit period will be 0.96ms. We use Baud rate 10417

    // Initial state
    appState = SM_IDLE;

    while(1)
    {
        ApplicationTask();
    }
}

/**
 * Interrupt Handler.
 */
void __interrupt tc(void) {

    const static uint8_t encodedPacketSize = sizeof(encodedPayload);

    // USART TX IRQ. Interrupt driven transmission.
    if (PIE1bits.TXIE && PIR1bits.TXIF)
    {
        appState = SM_TX_IN_PROGRESS; // Transmitting

        // Send preamble bytes first.
        if (!bPreambleSent)
        {
            TXREG = preamble[txPreambleCursor];

            // Preamble sent. Continue with payload.
            if (++txPreambleCursor >= sizeof(preamble))
            {
                bPreambleSent = 1;
                txPreambleCursor = 0;
            }
        }
        else{
            // Select byte to transmit & go.
            TXREG = encodedPayload[txPayloadCursor];

            // If last byte sent. Disable IRQ, otherwise program will return to IRQ handler endlessly
            if (++txPayloadCursor >= encodedPacketSize){
                txPayloadCursor = 0;
                bPreambleSent = 0;

                PIE1bits.TXIE = 0;
                appState = SM_TX_DONE;
            }
        }


    }


    // Timer 1. 16-bit timer.
    if (PIE1bits.TMR1IE && PIR1bits.TMR1IF)
    {
        // Respond only if in IDLE
        if (appState == SM_IDLE)
        {
            PIR1bits.TMR1IF = 0; // Clear IRQ flag
            T1CONbits.TMR1ON = 0; // Stop timer.
            ++appState;
            LED ^= 1;
        }

    }

    // External IRQ. Do nothing.
    if (INTCONbits.INTE && INTCONbits.INTF)
    {
        INTCONbits.INTF = 0;
    }

    // Timer0 IRQ. Do nothing.
    if (INTCONbits.T0IE && INTCONbits.T0IF)
    {
        // Clear flag
        INTCONbits.T0IF = 0;
    }
}

void ApplicationTask(void)
{
    switch (appState){
        case SM_IDLE:
            break;

        case SM_READY_TO_MEASURE:
            measure();

            // Ready to pack
            ++appState;
            break;

        case SM_READY_TO_ENCODE:

            // Encode data for Radio TX
            encodeForTx();
             ++appState;
            break;

        case SM_READY_TO_TX:

            

             if (TXSTAbits.TRMT){ // Check if no transmission taking place.
                PIE1bits.TXIE = 1; // This will trigger USART TX interrupt immediately if no data is currently being transmitted. INTCON.PEIE must set
             }
            break;

        case SM_TX_IN_PROGRESS:
            break;

        case SM_TX_DONE:
            // Preset TMR1 to 1 second value and re-enable Timer1
            TMR1 = 32767;
            T1CONbits.TMR1ON = 1;
            appState = SM_IDLE;
            //  asm("SLEEP");
            break;
    }

}