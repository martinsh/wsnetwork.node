/* 
 * File:   main.h
 * Author: Martins
 *
 * Created on ceturtdiena, 2014, 25 septembris, 20:34
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif

    #define LED_TRIS TRISCbits.TRISC2
    #define LED PORTCbits.RC2

    // 433MHz TX pin
    #define TX PORTBbits.RB5

    void ApplicationTask(void);

#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

