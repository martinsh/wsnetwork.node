#include "sensors.h"
#include <stdint.h>

#include <string.h>



uint8_t sensors[][8] = {
    {0x28, 0xc5, 0x9b, 0x03, 0x03, 0x00, 0x00, 0xc1}, // outdoors
    {0x28, 0xda, 0xa7, 0x03, 0x03, 0x00, 0x00, 0x1b} // indoors
};

// Reserve space on RAM
static PACKET packet = {
    0x1, // Node id
    {SENSOR_TYPE_TEMPERATURE, 0x1, NULL}, // DS18B20 #1
    {SENSOR_TYPE_TEMPERATURE, 0x2, NULL} // DS18B20 #2
};

// Encoded data. Reserved space in RAM.
uint8_t encodedPayload[ENCODED_PACKET_SIZE];

void measure(void) {

    // Compound literals
    uint8_t tmp[9] = {0x28, 0x26, 0x24, 0x22, 0x20, 0x1F, 0x1D, 0x1A, 0x19};
    memcpy(packet.sensor1.scratchpad, tmp, sizeof (tmp));
    memcpy(packet.sensor2.scratchpad, tmp, sizeof (tmp));
}

/**
 * Encode data suitable for radio transmission.
 * The basic idea is to encode 2 bytes to 3 bytes so that equal number of 0`s and 1`s are present in each byte leaving TX
 *
 * @return void
 */
void encodeForTx(void) {

    uint8_t *encPtr = encodedPayload;
    uint8_t* packetPtr = (uint8_t*) & packet;

    uint8_t i;
    static const uint8_t loopLimit = sizeof (encodedPayload) / 3 * 2;

    // Starting with 0, loop must end with uneven 'i' to properly complete 2byte -> 3byte conversion.
    for (i = 0; i < loopLimit; ++i) {

        uint8_t encNibbleLo, encNibbleHi = 0;

        // Last element. Out of packet boundaries.
        if (i >= sizeof (packet)) {
            encNibbleLo = lookup(0);
            encNibbleHi = lookup(0);
        } else {
            // Low nibble encoded. 6 valid bits.
            encNibbleLo = lookup(*packetPtr & 0b00001111);

            // High nibble encoded. 6 valid bits.
            encNibbleHi = lookup(*packetPtr >> 4);
        }

        // Uneven packet byte
        if (i & 1) {

            // Take encNibbleHi bits 3-6 and copy to Byte #2 bits 1-4
            *encPtr |= encNibbleHi >> 2;

            // Byte #2 completed
            ++encPtr;

            // Copy bits 5,6 from encNibbleHi to Byte #3 bits 7,8
            *encPtr = (encNibbleHi & 0b11) << 6;

            // Finish Byte #3 (the whole 2byte -> 3byte conversion) by adding encNibbleLo to Byte #3 bits 1-6
            *encPtr |= encNibbleLo;

            ++encPtr;


        } else { // Even packet byte

            *encPtr = encNibbleHi;
            *encPtr <<= 2;
            *encPtr |= (encNibbleLo >> 4); // Byte #1 completed
            ++encPtr;

            // Copy encNibbleLo bits 1-4 to Byte #2 Hi nibble
            *encPtr = (encNibbleLo & 0b1111) << 4;

            // Do nothing in this iteration. Wait for next - uneven iteration to complete Byte #2 bits 1-4
        }

        ++packetPtr;
    }
}

/**
 * Encoding Lookup table.
 * No Range check.
 *
 * @see http://embeddedgurus.com/stack-overflow/2010/01/a-tutorial-on-lookup-tables-in-c/
 *
 * @param  uint8_t index
 */
uint8_t lookup(uint8_t index) {

    // This is stored in program memory space (flash) due to const modifier
    static const uint8_t _lookup[16] = {
        0b000111, 0b001011, 0b001101, 0b001110,
        0b010011, 0b010101, 0b010110, 0b011001,
        0b011010, 0b011100, 0b100011, 0b100101,
        0b100110, 0b101001, 0b101010, 0b101100
    };

    return _lookup[index];
}
