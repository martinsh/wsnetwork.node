/* 
 * File:   sensors.h
 * Author: Martins
 *
 * Created on ceturtdiena, 2014, 30 oktobris, 20:24
 */

#ifndef SENSORS_H
#define	SENSORS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>

// Define sensor types
#define SENSOR_TYPE_TEMPERATURE (0x01)
#define SENSOR_TYPE_HUMIDITY    (0x02)

void measure(void);
void encodeForTx(void);
uint8_t lookup(uint8_t);


// DS18B20 Temperature sensor.
typedef struct {
    uint8_t     SensorType; // Sensor type
    uint8_t     SensorId;
    uint8_t     scratchpad[9];
} SENSOR_DS18B20;

// HH10D humidity sensor
typedef struct {
    uint8_t     SensorType; // Sensor type
    uint8_t     SensorId;
    uint8_t     data[4];
} SENSOR_HH10D;

typedef struct {
    uint8_t        NodeId; // TX Node Id
    SENSOR_DS18B20 sensor1;
    SENSOR_DS18B20 sensor2;
} PACKET;

#define ENCODED_PACKET_SIZE ((sizeof(PACKET) & 1) ? (sizeof(PACKET) + 1)/2*3  : sizeof(PACKET)/2*3)

extern uint8_t encodedPayload[ENCODED_PACKET_SIZE];


#ifdef	__cplusplus
}
#endif

#endif	/* SENSORS_H */

